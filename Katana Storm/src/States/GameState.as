package States 
{
	import org.flixel.FlxState;
	import org.flixel.FlxText;
	import Units.Unit;
	
	/**
	 * ...
	 * @author Mclean Oshiokpekhai
	 */
	public class GameState extends FlxState 
	{
		
		var TestUnit:Unit = new Unit();
		
		public function GameState() 
		{
			add(new FlxText(10, 10, 100, "Hello World!"));
		}
		
	}

}