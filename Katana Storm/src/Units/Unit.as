package Units 
{
	import Items.Backpack;
	import Items.Body;
	import org.flixel.FlxObject;
	import org.flixel.FlxSprite;
	/**
	 * ...
	 * @author ...
	 */
	public class Unit extends FlxSprite
	{
		//stats
		public var level:int;
		public var strength:int;
		public var speed:int;
		public var magic:int;
		public var defense:int;
		
		//class 
		public var background:String;
		
		//name
		public var name:String;
		
		
		//items
		var backpack:Backpack;
		
		//equipped items
		var equipped:Body;
		
		public function Unit() 
		{
			//health starts at 100 for everyone
			this.health = 100;
			
			backpack = new Backpack();
			backpack.maxSize = 7;
		}
		
	}

}