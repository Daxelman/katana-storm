package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import org.flixel.FlxGame;
	import States.GameState;
	[SWF(width = "1280", height = "720", backgroundColor = "#000000")]
	/**
	 * ...
	 * @author Mclean Oshiokpekhai
	 */
	public class Main extends FlxGame
	{
		
		public function Main():void 
		{
			super(1280, 720, GameState, 1);
		}
		
	}
	
}